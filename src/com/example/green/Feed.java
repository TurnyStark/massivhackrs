package com.example.green;

/**
 * Created by wvg226 on 01/11/2014.
 */
public class Feed {
    private int category = 0;

    public Feed()
    {

    }

    public Feed(int category) {
        this.category = category;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }
}
