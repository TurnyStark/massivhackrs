package com.example.green;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wvg226 on 01/11/2014.
 */
public class MyAdapter extends BaseAdapter{
    private final LayoutInflater inflater;
    private final List<Feed> feeds = new ArrayList<Feed>();

    public MyAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return feeds.size();
    }

    @Override
    public Object getItem(int position) {
        return feeds.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        if(feeds.get(position).getCategory()==0){
            view = inflater.inflate(R.layout.achievments, null);

        } else {
            view = inflater.inflate(R.layout.activities, null);
        }

        return view;
    }

    public void replaceWith(List<Feed> feedList) {
        this.feeds.clear();
        this.feeds.addAll(feedList);
        notifyDataSetChanged();
    }
}
